import 'package:flutter/material.dart';

import './Question.dart';
import './answer.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questiontext': 'whats your fav color',
      'answers': [
        {'text': 'red', 'score': 10},
        {'text': 'green', 'score': 20},
        {'text': 'blue', 'score': 30},
        {'text': 'black', 'score': 40}
      ]
    },
    {
      'questiontext': 'whats your fav food',
      'answers': [
        {'text': 'chicken', 'score': 10},
        {'text': 'egg', 'score': 20},
        {'text': 'allu', 'score': 30},
        {'text': 'fish', 'score': 40}
      ]
    },
    {
      'questiontext': 'whats your fav animal',
      'answers': [
        {'text': 'lion', 'score': 10},
        {'text': 'cheetha', 'score': 20},
        {'text': 'dog', 'score': 30},
        {'text': 'rabbit', 'score': 40}
      ]
    }
  ];
  var _questionIndex = 0;
  var _totalScore = 0;
  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            'My First App',
          ),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore,_resetQuiz),
      ),
    );
  }
}
